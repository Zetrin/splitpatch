#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>


off_t find(const char *data, int index, const size_t total) {
	while (index < total ) {
		if (memcmp("diff ", &data[index], 5) == 0)
			return index;

		while (index < total && data[index] != '\n')
			index++;

		index += 1;
	}

	return -1;
}


off_t find_next(const char *data, int index, const size_t total) {
	while (index < total) {
		if (memcmp("\ndiff ", &data[index], 6) == 0)
			return index + 1;

		if (memcmp("\n-- \n", &data[index], 5) == 0)
			return index + 1;

		index += 1;

		while (index < total && data[index] != '\n')
			index++;
	}

	return total - 1;
}


int main(int argc, char *argv[]) {
	int source_file, out_file, index = 0;
	char *in_buffer, out_filename[FILENAME_MAX];
	off_t source_offset = 0;
	size_t patch_id = 0, out_size, ret;
	off_t from = 0, to = -1;
	char *p;
	struct stat sb;


	if (argc != 2) {
		fprintf(stderr, "Usage: %s <pathname>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	source_file = open(argv[1], O_RDONLY | O_NOFOLLOW, NULL);
	if (source_file == -1) {
		perror("Could not open file");
		exit(EXIT_FAILURE);
	}

	fstat(source_file, &sb);

	in_buffer = malloc(sb.st_size);
	if (in_buffer == NULL) {
		perror("Failed to allocate memory");
		close(source_file);
		exit(EXIT_FAILURE);
	}

	ret = read(source_file, in_buffer, sb.st_size);
	if (ret == -1 || ret != sb.st_size) {
		perror("Failed to read file");
		close(source_file);
		free(in_buffer);
		exit(EXIT_FAILURE);
	}

loop:
	from = find(in_buffer, index, sb.st_size);
	if (from == -1)
		goto end;

	to = find_next(in_buffer, from, sb.st_size);

	out_size = to - from;

	sprintf(out_filename, "%04lu.patch", patch_id);
	out_file = open(out_filename, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
	if (out_file == 0) {
		fprintf(stderr, "Could not open file %s: %s\n", out_filename, strerror(errno));
		goto skip;
	}

	ret = write(out_file, &in_buffer[from], out_size);
	if (ret != out_size) {
		fprintf(stderr, "Could not write file %s: %s\n", out_filename, strerror(errno));
	}

	close(out_file);

skip:
	patch_id += 1;
	index = to;

	if (index < sb.st_size - 1)
		goto loop;

end:
	free(in_buffer);
	close(source_file);

	return EXIT_SUCCESS;
}
